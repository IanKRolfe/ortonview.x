==========================
Ortonview Firmware Archive
==========================

This repository serves as an archive of the firmware versions published
by Karen Orton on the Vintage Radio Repair and Restoration Forum thread 
https://www.vintage-radio.net/forum/showthread.php?t=162766
and developments subsequent to that.

Versions are tagged "fw###" where ### is the post number in the thread
where the zip file was posted. It is possible to diff the versions, 
I am not sure however if Karen based each subsequent version on the 
previous one, but I have assumed she did. However if you do diff two
different versions and get a mess, then perhaps she didn't!

Future versions may be tagged under a different scheme...

**NOTE**:
The firmware has been modified in the "PIC16F887" branch to use the newer and in
current production PIC16F887 chip, which is intended to be the chip to use, and will
be merged back into "master" when testing is complete.

If you wish to use the firmware developed for the original PIC 16F877 then use
the tag last877 to check out that version, and branch from there.

From this point on the firmware version numbers will be released as r2.### tag
rather than the old fw### tags because linking them to forum posts is not really
useful any more. If there are relevent forum posts, perhaps put them in the commit
comments.
